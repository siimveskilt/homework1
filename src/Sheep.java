import java.util.Arrays;
import java.util.Random;

public class Sheep {

	// Bitbucket link: https://bitbucket.org/siimveskilt/homework1

	enum Animal {
		sheep, goat
	};

	public static void main(String[] param) {
		final int NUM_OF_ANIMALS = 10;
		Animal[] animals = new Animal[NUM_OF_ANIMALS];

		for (int i = 0; i < NUM_OF_ANIMALS; i++) {
			animals[i] = randomAnimal();
		}

		System.out.println(Arrays.asList(animals));
		reorder(animals);
		System.out.println(Arrays.asList(animals));

	}

	/**
	 * @param animals
	 *            is the array of animals to be reordered.
	 */
	public static void reorder(Animal[] animals) {

		// sweep from front for sheep
		int firstSheepIndex = findSheepIndex(animals, 0);
		// and from back for goats
		int firstGoatIndex = findGoatIndex(animals, animals.length - 1);

		if (firstSheepIndex == -1 || firstGoatIndex == -1)
			return; // nothing to reorder

		// keep swapping while condition holds
		while (firstGoatIndex > firstSheepIndex) {
			// swap sheep with goat
			swap(animals, firstSheepIndex, firstGoatIndex);
			// update index's
			firstGoatIndex = findGoatIndex(animals, firstGoatIndex - 1);
			firstSheepIndex = findSheepIndex(animals, firstSheepIndex + 1);
		}
	}

	/**
	 * @param animals
	 *            is the array of animals where the swap happens.
	 * @param firstSheepIndex
	 *            is the index to the sheep to be swapped.
	 * @param firstGoatIndex
	 *            is the index to the goat to be swapped.
	 */
	private static void swap(Animal[] animals, int firstSheepIndex, int firstGoatIndex) {
		Animal temp = animals[firstSheepIndex];
		animals[firstSheepIndex] = animals[firstGoatIndex];
		animals[firstGoatIndex] = temp;
	}

	/**
	 * @return an array of animals in random order.
	 */
	static private Animal randomAnimal() {
		return Animal.values()[new Random().nextInt(Animal.values().length)];
	}

	/**
	 * @param animals
	 *            is the array of animals where the method looks for the sheep.
	 * @param firstIndex
	 *            is the index where we first might encounter our sheep.
	 * @return the index of the sheep or -1 if no sheep was found.
	 */
	static private int findSheepIndex(Animal[] animals, int firstIndex) {
		for (int i = firstIndex; i < animals.length; i++) {
			if (animals[i] == Animal.sheep) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * @param animals
	 *            is the array of animals where the method looks for the goat.
	 * @param firstIndex
	 *            is the index where we first might encounter our goat.
	 * @return the index of the goat or -1 if no goat was found.
	 */
	static private int findGoatIndex(Animal[] animals, int firstIndex) {
		for (int i = firstIndex; i > 0; i--) { // start from the back
			if (animals[i] == Animal.goat) {
				return i;
			}
		}
		return -1;
	}
}
